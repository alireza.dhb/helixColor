var Game = Game || {};
let clock = new THREE.Clock();
let mixer ;
let gameObjects=[];
let ball;
let cylinderGroup;
let endPosition = 190 ;
let colorChanger;
let colorChanged = false;
let video;

Game.USE_WIREFRAME = false; // create a constant so that we can toggle between wireframe
Game.GAME_LOADED = false;
Game.GAME_STARTED = false;
Game.GAME_ENDED = false;

Game.player = {
    speed: 0.2,
    rotateSpeed: Math.PI * 0.01,
    wallDistance : 20
};

let ratio = window.devicePixelRatio || 1;
let w = screen.width * ratio;
let h = screen.height * ratio;


function update() {
    requestAnimationFrame(update);
    Game.renderer.render(Game.scene, Game.camera);
    Game.updateInput();
    if ( mixer ) mixer.update( clock.getDelta() );
    Game.checkCollision();
    //Game.updateCamera();
}

Game.init = function() {
    this.scene = new THREE.Scene();
    window.scene=this.scene;

    video = document.getElementById('video');
    video.play();
    let bgTex = new THREE.VideoTexture(video);
    //bgTex.wrap = THREE.RepeatWrapping;
    // var bgMesh = new THREE.Mesh(
    //     new THREE.PlaneGeometry(30, 10, 5, 5),
    //     new THREE.MeshBasicMaterial({
    //         map:bgTex
    //
    //     })
    // );
    //
    // bgMesh.rotation.set(Math.PI+.64  ,0,0);
    // //bgMesh.rotation.y += Math.PI+1;
    // bgMesh.receiveShadow = true;
    // bgMesh.position.set(0, 0, 8);
    // scene.add(bgMesh);


    //scene.background = bgTex;
    this.camera = new THREE.PerspectiveCamera(50, 350 / 550, 0.01, 1000);

    this.addLights();
    this.camera.position.set(0, 6, -8);
    this.camera.lookAt(new THREE.Vector3(0, 0, 8));
    this.renderer = new THREE.WebGLRenderer({ antialias: true });
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(350, 550);
    document.body.appendChild(this.renderer.domElement);

    document.addEventListener('mousemove', function(event) {
        mouseLoc = event.clientX;
    }, false);
    document.body.addEventListener("mousedown", function(event) {
        mouseStart = event.clientX;
        mouseDown = true
    }, false);
    document.body.addEventListener("mouseup", function(event) {
        mouseDown = false
    }, false);

    document.addEventListener('touchmove', function(event) {
        mouseLoc = event.touches[0].clientX;
    }, false);
    document.body.addEventListener("touchstart", function(event) {
        mouseDown = true
    }, false);
    document.body.addEventListener("touchend", function(event) {
        mouseDown = false
    }, false);
    document.body.addEventListener("touchcancel",function(event) {
        mouseDown = false
    }, false);

    this.loadResources();
    update();
};

Game.addLights = function() {
    // LIGHTS
    var ambientLight = new THREE.AmbientLight(0xffffff,1);
    this.scene.add(ambientLight);
        this.light = new THREE.PointLight(0xffffff, 1, 18);
        this.light.position.set(0, 3, 10);

        //this.scene.add(this.light);
};

Game.loadResources = function() {


    // cylinder group is added, all platforms will be added to it
    this.cylinderGroup = new THREE.Group();
    cylinderGroup = this.cylinderGroup;



    // let roadLoader = new THREE.OBJLoader2();
    // roadLoader.loadMtl( 'Models/'+ "road" +'.mtl', null, function ( materials ) {
    //     roadLoader.setModelName( "road" );
    //     roadLoader.setMaterials( materials );
    //     roadLoader.load( 'Models/'+ "road" +'.obj', function ( event ) {
    //         event.detail.loaderRootNode.position.set(0,0,80);
    //         event.detail.loaderRootNode.rotation.z=Math.PI/2;
    //         event.detail.loaderRootNode.scale.set(1,1,80);
    //         cylinderGroup.add(event.detail.loaderRootNode );
    //     }, null, null, null, false )
    // });
    let roadLoader =  new THREE.GLTFLoader;
    roadLoader.load("Models/road.glb", function (gltf) {
        model = gltf.scene;
        model.rotation.z = Math.PI/2;
        model.position.set(0,0,110);
        model.scale.set(1.7,1.7,10);
        cylinderGroup.add(model);
    });
    let texture = new THREE.TextureLoader().load( 'Models/Balls/blueBall.jpg');
    var material = new THREE.MeshBasicMaterial({map: texture});
    this.ball = new THREE.Mesh(
        new THREE.SphereGeometry(0.3,20,20)
    );
    this.ball.receiveShadow=true;
    this.ball.position.set(0,2.2,0);
    this.ball.material = material;
    ball = this.ball;
    ball.mat = "blue";
    this.scene.add(this.ball);

    let loader = new THREE.GLTFLoader();
    let model;
    loader.load( 'Models/HalfWallBlue.gltf', function ( gltf ) {
        model = gltf.scene;
        model.rotation.y = Math.PI;
        model.animations = gltf.animations;
        model.position.set(0,0,Game.player.wallDistance*2);
        model.minAngle = -Math.PI/2;
        model.maxAngle = Math.PI/2;
        model.mat="blue";

        let model2 = model.clone();
        model2.rotation.set(0,0,Math.PI);
        model2.position.set(0,0,Game.player.wallDistance);
        model2.minAngle = Math.PI/2;
        model2.maxAngle = 3*Math.PI/2;
        model2.animations = gltf.animations;
        model2.mat="blue";


        gameObjects.push(model);
        gameObjects.push(model2);

        cylinderGroup.add(model);
        cylinderGroup.add(model2);
    });

    let endLoader = new THREE.OBJLoader2();
    endLoader.loadMtl("Models/end.mtl",null , function (material) {
        endLoader.setMaterials( material );
        endLoader.load( 'Models/'+ "end" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(0,0,190);
            event.detail.loaderRootNode.scale.set(1.73,1.73,3);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )    })

    let greenHalfLoader = new THREE.OBJLoader2();
    greenHalfLoader.loadMtl( 'Models/'+ "HalfWallGreen" +'.mtl', null, function ( materials ) {
        greenHalfLoader.setModelName( "HalfWallGreen" );
        greenHalfLoader.setMaterials( materials );
        greenHalfLoader.load( 'Models/'+ "HalfWallGreen" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(0,0,Game.player.wallDistance);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });

    let purpleHalfLoader = new THREE.OBJLoader2();
    purpleHalfLoader.loadMtl( 'Models/'+ "HalfWallPurple" +'.mtl', null, function ( materials ) {
        purpleHalfLoader.setModelName( "HalfWallPurple" );
        purpleHalfLoader.setMaterials( materials );
        purpleHalfLoader.load( 'Models/'+ "HalfWallPurple" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(0,0,2*Game.player.wallDistance);
            event.detail.loaderRootNode.rotation.set(0,0,Math.PI);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });

    let colorlessLoader = new THREE.OBJLoader2();
    colorlessLoader.loadMtl( 'Models/'+ "Colorless" +'.mtl', null, function ( materials ) {
        colorlessLoader.setModelName( "Colorless" );
        colorlessLoader.setMaterials( materials );
        colorlessLoader.load( 'Models/'+ "Colorless" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(0,0,3*Game.player.wallDistance);
            let cloned = event.detail.loaderRootNode.clone();
            cloned.position.set(0,0,9*Game.player.wallDistance);
            cylinderGroup.add(event.detail.loaderRootNode );
            cylinderGroup.add(cloned );

        }, null, null, null, false )
    });

    let threeSlice1_blue = new THREE.OBJLoader2();
    threeSlice1_blue.loadMtl( 'Models/'+ "ThirdWallBlue" +'.mtl', null, function ( materials ) {
        threeSlice1_blue.setModelName( "ThirdWallBlue" );
        threeSlice1_blue.setMaterials( materials );
        threeSlice1_blue.load( 'Models/'+ "ThirdWallBlue" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(0,0,4*Game.player.wallDistance);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });

    let threeSlice1_green = new THREE.OBJLoader2();
    threeSlice1_green.loadMtl( 'Models/'+ "ThirdWallGreen" +'.mtl', null, function ( materials ) {
        threeSlice1_green.setModelName( "ThirdWallGreen" );
        threeSlice1_green.setMaterials( materials );
        threeSlice1_green.load( 'Models/'+ "ThirdWallGreen" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(0,0,4*Game.player.wallDistance);
            event.detail.loaderRootNode.rotation.set(0,0,Math.PI*2/3);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });

    let OrangeLoader = new THREE.GLTFLoader();
    OrangeLoader.load( 'Models/thirdWallOrange.gltf', function ( gltf ) {
        model = gltf.scene;
        model.rotation.y = Math.PI;
        model.animations = gltf.animations;
        model.position.set(0,0,Game.player.wallDistance*4);
        model.rotation.set(0,0,-Math.PI*2/3);
        model.minAngle = -Math.PI*2/3;
        model.maxAngle = 0;

        let model2 = model.clone();
        model2.position.set(0,0,6*Game.player.wallDistance);
        model2.rotation.set(0,0,Math.PI*2/3);
        model2.minAngle = 0;
        model2.maxAngle = Math.PI*2/3;

        cylinderGroup.add(model);
        cylinderGroup.add(model2);
    });

    let Orange2Loader = new THREE.GLTFLoader();
    Orange2Loader.load( 'Models/quarterWallOrange.gltf', function ( gltf ) {
        model = gltf.scene;
        model.rotation.y = Math.PI;
        model.animations = gltf.animations;
        model.position.set(0,0,Game.player.wallDistance*7);
        model.rotation.set(0,0,Math.PI);
        //model.minAngle = -Math.PI*2/3;
        //model.maxAngle = 0;

        let model2 = model.clone();
        model2.position.set(0,0,8*Game.player.wallDistance);
        model2.rotation.set(0,0,Math.PI/2);
        //model2.minAngle = 0;
        //model2.maxAngle = Math.PI*2/3;

        cylinderGroup.add(model);
        cylinderGroup.add(model2);
    });
        // let threeSlice1_Orange = new THREE.OBJLoader2();
    // threeSlice1_Orange.loadMtl( 'Models/'+ "ThirdWallOrange" +'.mtl', null, function ( materials ) {
    //     threeSlice1_Orange.setModelName( "ThirdWallOrange" );
    //     threeSlice1_Orange.setMaterials( materials );
    //     threeSlice1_Orange.load( 'Models/'+ "ThirdWallOrange" +'.obj', function ( event ) {
    //         event.detail.loaderRootNode.position.set(0,0,5*Game.player.wallDistance);
    //         event.detail.loaderRootNode.rotation.set(0,0,-Math.PI*2/3);
    //         cylinderGroup.add(event.detail.loaderRootNode );
    //     }, null, null, null, false )
    // });


    colorChanger = new THREE.Mesh(
        new THREE.CylinderGeometry(1, 1, 1 , 50),
        new THREE.MeshPhongMaterial({ wireframe: false, color: 0x118d6a })
    );
    colorChanger.position.set(0, 0, 5*Game.player.wallDistance);
    colorChanger.rotation.x=Math.PI/2;
    colorChanger.scale.set(1.9,.5,1.9);
    this.cylinderGroup.add(colorChanger);

    let threeSlice2_purple = new THREE.OBJLoader2();
    threeSlice2_purple.loadMtl( 'Models/'+ "ThirdWallPurple" +'.mtl', null, function ( materials ) {
        threeSlice2_purple.setModelName( "ThirdWallPurple" );
        threeSlice2_purple.setMaterials( materials );
        threeSlice2_purple.load( 'Models/'+ "ThirdWallPurple" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(0,0,6*Game.player.wallDistance);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });

    let threeSlice2_green = new THREE.OBJLoader2();
    threeSlice2_green.loadMtl( 'Models/'+ "ThirdWallGreen" +'.mtl', null, function ( materials ) {
        threeSlice2_green.setModelName( "ThirdWallGreen" );
        threeSlice2_green.setMaterials( materials );
        threeSlice2_green.load( 'Models/'+ "ThirdWallGreen" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(0,0,6*Game.player.wallDistance);
            event.detail.loaderRootNode.rotation.set(0,0,-Math.PI*2/3);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });

    let fourSlice1_green = new THREE.OBJLoader2();
    fourSlice1_green.loadMtl( 'Models/'+ "quarterWallGreen" +'.mtl', null, function ( materials ) {
        fourSlice1_green.setModelName( "quarterWallGreen" );
        fourSlice1_green.setMaterials( materials );
        fourSlice1_green.load( 'Models/'+ "quarterWallGreen" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(.1,-.1,7*Game.player.wallDistance);
            event.detail.loaderRootNode.rotation.set(0,0,Math.PI/2);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });

    let fourSlice1_blue = new THREE.OBJLoader2();
    fourSlice1_blue.loadMtl( 'Models/'+ "quarterWallBlue" +'.mtl', null, function ( materials ) {
        fourSlice1_blue.setModelName( "quarterWallBlue" );
        fourSlice1_blue.setMaterials( materials );
        fourSlice1_blue.load( 'Models/'+ "quarterWallBlue" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(-.1,0,7*Game.player.wallDistance);
            event.detail.loaderRootNode.rotation.set(0,0,-Math.PI/2);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });

    let fourSlice1_purple = new THREE.OBJLoader2();
    fourSlice1_purple.loadMtl( 'Models/'+ "quarterWallPurple" +'.mtl', null, function ( materials ) {
        fourSlice1_purple.setModelName( "quarterWallPurple" );
        fourSlice1_purple.setMaterials( materials );
        fourSlice1_purple.load( 'Models/'+ "quarterWallPurple" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(0,-.2,7*Game.player.wallDistance);
            event.detail.loaderRootNode.rotation.set(0,0,0);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });



    let fourSlice2_green = new THREE.OBJLoader2();
    fourSlice2_green.loadMtl( 'Models/'+ "quarterWallGreen" +'.mtl', null, function ( materials ) {
        fourSlice2_green.setModelName( "quarterWallGreen" );
        fourSlice2_green.setMaterials( materials );
        fourSlice2_green.load( 'Models/'+ "quarterWallGreen" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(.1,.1,8*Game.player.wallDistance);
            event.detail.loaderRootNode.rotation.set(0,0,Math.PI);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });

    let fourSlice2_blue = new THREE.OBJLoader2();
    fourSlice2_blue.loadMtl( 'Models/'+ "quarterWallBlue" +'.mtl', null, function ( materials ) {
        fourSlice2_blue.setModelName( "quarterWallBlue" );
        fourSlice2_blue.setMaterials( materials );
        fourSlice2_blue.load( 'Models/'+ "quarterWallBlue" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(-.1,0,8*Game.player.wallDistance);
            event.detail.loaderRootNode.rotation.set(0,0,-Math.PI/2);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });

    let fourSlice2_purple = new THREE.OBJLoader2();
    fourSlice2_purple.loadMtl( 'Models/'+ "quarterWallPurple" +'.mtl', null, function ( materials ) {
        fourSlice2_purple.setModelName( "quarterWallPurple" );
        fourSlice2_purple.setMaterials( materials );
        fourSlice2_purple.load( 'Models/'+ "quarterWallPurple" +'.obj', function ( event ) {
            event.detail.loaderRootNode.position.set(0,-.2,8*Game.player.wallDistance);
            event.detail.loaderRootNode.rotation.set(0,0,0);
            cylinderGroup.add(event.detail.loaderRootNode );
        }, null, null, null, false )
    });

    this.scene.add(this.cylinderGroup);


};



Game.addLoader = function() {

    var progress = document.createElement('div');
    progress.setAttribute("id", "loader");
    var progressBar = document.createElement('div');
    progressBar.setAttribute("id", "bar");
    progress.appendChild(progressBar);
    document.body.appendChild(progress);

    this.loadingManager = new THREE.LoadingManager();
    this.loadingManager.onProgress = function(item, loaded, total) {
        progressBar.style.width = (loaded / total * 100) + '%';
        console.log(item, loaded, total);
    };
    this.loadingManager.onLoad = function() {
        console.log("loaded all resources");
        !Game.GAME_LOADED && document.body.removeChild(progress);
        Game.GAME_LOADED = true;
        Game.GAME_STARTED = true;
        Game.onResourcesLoaded();
    };
};


Game.updateCamera = function() {
    if (this.ball.position.z < endPosition && !Game.GAME_ENDED){
        this.camera.position.z+=Game.player.speed;
        this.ball.position.z+=Game.player.speed;
    }

};

Game.checkCollision = function () {
    for(let i=0; i<gameObjects.length;i++){
        let object = gameObjects[i];
        let ballMinBoundZ = ball.position.z + ball.scale.z ;
        let wall1 = object.position.z - object.scale.z ;

        if(ballMinBoundZ >= colorChanger.position.z && !colorChanged)
        {
            ball.material = new THREE.MeshBasicMaterial({map: new THREE.TextureLoader().load( 'Models/Balls/greenBall.png')});
            ball.mat="green";
            colorChanged=true;
        }

        if (ballMinBoundZ >= wall1){
            //console.error(cylinderGroup.rotation.z + " , " + object.minAngle +" ," +  object.maxAngle )
        }
        //@todo add translation for angles
        if (ballMinBoundZ >= wall1 &&
            cylinderGroup.rotation.z > object.minAngle &&
            cylinderGroup.rotation.z < object.maxAngle){
            if(ball.mat == object.mat){
                playAnimation(object);
                gameObjects.splice(i,1);
                break;
            }
            else
                defeat();
        }
    }

    function playAnimation(object){
        mixer= new THREE.AnimationMixer( object );
        for (let i=0;i<object.animations.length;i++){
            let animation =mixer.clipAction( object.animations[ i ] );
            animation.setLoop(THREE.LoopOnce);
            animation.clampWhenFinished = true;
            animation.timeScale = 4;
            animation.play();
        }
    }
    function defeat() {
        ball.visible=false;
        Game.GAME_ENDED = true;
    }
};

Game.onResourcesLoaded = function() {
    //video.play();
};
function playAnimation(object){
    mixer= new THREE.AnimationMixer( object );
    for (let i=0;i<object.animations.length;i++){
        let animation =mixer.clipAction( object.animations[ i ] );
        animation.setLoop(THREE.LoopOnce);
        animation.clampWhenFinished = true;
        animation.timeScale = 4;
        animation.play();
    }
}
var screenW = window.innerWidth;
var screenH = window.innerHeight; /*SCREEN*/
let spdx = 0, mouseLoc = 0, mouseStart = 0, mouseDown = false; /*MOUSE*/

window.onload = function() {
    Game.init();
};


Game.updateInput = function () {
    spdx = -(mouseLoc-mouseStart)/10;

    if (mouseDown){
        this.cylinderGroup.rotation.z = -spdx;
    }

};


